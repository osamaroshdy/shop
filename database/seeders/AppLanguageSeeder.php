<?php

namespace Database\Seeders;

use App\Models\AppLanguage;
use Illuminate\Database\Seeder;

class AppLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AppLanguage::create([
            'name' => 'English',
            'locale' => 'en',
            'icon' => 'en.svg',
            'direction' => 'ltr',
            'sorting' => '1',
            'front' => '1',
            'back' => '1',
        ]);
        AppLanguage::create([
            'name' => 'Arabic',
            'locale' => 'ar',
            'icon' => 'ar.svg',
            'direction' => 'rtl',
            'sorting' => '2',
            'front' => '1',
            'back' => '1',
        ]);
    }
}
