<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class Language
{

    public function handle($request, Closure $next)
    {
        App::setLocale( Cookie::get('applocale') ?: config('app.locale') );

        return $next($request);
    }
}
